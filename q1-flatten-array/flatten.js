// var arr = [1];
// for (var i=0; i<100000; i++) {
// 	arr = [arr,1]
//}
var arr = [[1,2],3,[4],[5,[7,8]]]
function flatten(arr) {
	var flattened = arr.slice()
	var i = 0
	while(i < flattened.length) {
		if(flattened[i].constructor === Array) {
			var segment = flattened[i]
			flattened.splice(i, 1, segment[0])
			for (var j = 1; j < segment.length; j++) {
				flattened.splice(i+j, 0, segment[j])
			}
		} else {
			i++
		}
	}
	return flattened
}

console.log(flatten(arr))
