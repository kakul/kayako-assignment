Q1.

Instructions:

    node flatten.js
    
The code outputs the flattened array for a small input  
use the array generated in the commented block to test for a higher value

Q2.

Instructions:

    npm install
    node index.js

The code creates an Angular-JS directive like dom uelement `<upload></upload>` 
which has attributes that define the url, file, params, MIME-type, to be sent while
uploading. These can be modified directly by changing the attribute value or by
using Uploader.setConfig() function.
Supported attributes:
    url - Upload url
    type - file type
    method - POST/GET
    params - userid=1&parm2=asd
    
    
    