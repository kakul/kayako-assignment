var Uploader = {
	config : {
		url: '',
		method: 'POST',
		type: ['[a-z/]+'],
		params: '' 
	},
	upload:upload,
	previewImage:previewImage,
	setConfig:setConfig,
	createFilePicker:createFilePicker,
	createForm:createForm,
	createUploadButton:createUploadButton,
	createInput:createInput,
	changeUploadButtonText:changeUploadButtonText,
	getOptions:getOptions,
	showError:showError,
	checkTypeError:checkTypeError,
	checkIfImage:checkIfImage,
	changeStatus:changeStatus,
	createFileNode:createFileNode,
	createStatusBox:createStatusBox,
	parseParams:parseParams,
	initialize:initialize
}

function upload(file, config) {
	var request = new XMLHttpRequest();
	var formData = parseParams(config.params);
	formData.append(file.name, file);
	return new Promise(function(resolve, reject) {
		request.open(config.method, config.url);
		request.responseType = 'json';
		request.onload = function() {
			if (request.status === 200) {
				resolve(request.response);
			} else {
				reject(Error(request.statusText));
			}
		};
		request.onerror = function() {
			reject(Error('Network Error'));
		};
		request.send(formData);
	});
}

function previewImage(file) {
	var imgBox  = document.createElement('div');
	var img = document.createElement('img');
	var reader = new FileReader();
	var imgComponent = { container: imgBox, image: img }
	reader.onload = function(e) {
		img.setAttribute('src',e.target.result);
		img.style.height = '100%';
		img.style.width = '100%';
	}

	reader.readAsDataURL(file);
	imgBox.appendChild(img);
	return imgComponent;
}

function setConfig(options) {
	for (var key in options) {
		if(Uploader.config.hasOwnProperty(key)) {
			Uploader.config[key] = options[key];
		}
	}
}

function createFilePicker() {
	var filePicker = document.createElement('input');
	filePicker.setAttribute('type', 'file');
	filePicker.style.display = 'none';
	return filePicker;
}

function createForm() {
	var form = document.createElement('form');
	return form;
}

function createUploadButton() {
	var uploadButton = document.createElement('span');
	uploadButton.className = 'btn btn-default';
	uploadButton.innerHTML = '<span>Choose File</span>'
	return uploadButton;	
}

function createInput(placeHolder) {
	var elem = document.createElement('input');
	elem.setAttribute('type', 'text');
	return elem
}

function changeUploadButtonText(uploadButton, text) {
	uploadButton.innerHTML = '<span>' + text + '</span>';
}

function getOptions(uploadWidget) {
	var options = {};
	var attributes = uploadWidget.attributes;
	for(var i = 0; i < attributes.length; i++) {
		options[attributes[i].name] = attributes[i].nodeValue;
	}
	if (options['type']) {
		var typesArray = options['type'].split(',');
		options['type'] = typesArray;
	}
	return options;
}

function showError(ErrorMsg) {
	alert('Error:' + ErrorMsg);
}

function checkTypeError(Uploader, filePicker) {
	var MIMEType = Uploader.config.type;
		for (var type in MIMEType) {
			if(RegExp(MIMEType[type]).test(filePicker.files[0].type)) {
				return false;
			}
		}
		return true;
}

function checkIfImage(filePicker) {
	if (RegExp('image/*').test(filePicker.files[0].type)) {
		return true;
	}
	return false;
}
function changeStatus(statusMsg, statusBox) {
	statusMsg.container.innerHTML = '<div>' + statusMsg + '</div>';
}

function createFileNode(file) {
	var container = document.createElement('div');
	var paragraph = document.createElement('p');
	paragraph.innerHTML = file.name;
	container.appendChild(paragraph);
	var clearButton = document.createElement('span');
	clearButton.className = 'btn btn-default';
	clearButton.innerHTML = '<span>Remove</span>';
	container.appendChild(clearButton);
	var elements = {
		container: container,
		clearButton: clearButton,
		text: paragraph
	}
	return elements;
}

function createStatusBox() {
	var row = document.createElement('div');
	row.className = 'row';
	var container = document.createElement('div');
	container.className = 'col-sm-8';
	row.appendChild(container);
	var elements = { row:row, container: container };
	return elements;
}

function parseParams(params) {
	var params = params || '';
	var formData = new FormData();
	var paramsArr = params.split('&');
	for (i in paramsArr) {
		var param = paramsArr[i].split('=');
		formData.append(param[0],param[1]);
	}
	return formData;
}

function initialize() {
	var uploadWidget = document.getElementsByTagName('upload')[0];
	var filePicker = createFilePicker();
	var uploadButton = createUploadButton();
	var statusBox = createStatusBox();
	var form = createForm();
	var fileNode;

	var status = 'unattached';

	form.appendChild(filePicker);
	uploadWidget.appendChild(uploadButton);
	uploadWidget.appendChild(statusBox.row);

	uploadButton.addEventListener('click', function() {
		if (status === 'unattached') {
			statusBox.container.innerHTML = '';
			filePicker.click();	
		}
		if (status === 'attached') {
			upload(filePicker.files[0], Uploader.config).then(function(response){
				status = 'unattached';
				statusBox.container.innerHTML = '<div>Upload successful!</div>'
			}, function(Error) {
				showError(Error);
			});
		}
	});
	
	filePicker.addEventListener('change', function() {
		if(checkTypeError(Uploader, filePicker)) {
			showError("Invalid Type");
		} else {
			var imgComponent = undefined;
			if(checkIfImage(filePicker)) {
				var image = filePicker.files[0];
				imgComponent = previewImage(image);
				statusBox.container.appendChild(imgComponent.container);
			}
			var fileNode = createFileNode(filePicker.files[0]);
			statusBox.container.appendChild(fileNode.container);
			changeUploadButtonText(uploadButton, 'Upload');
			status = 'attached';
			fileNode.clearButton.addEventListener('click', function() {
				fileNode.container.remove();
				form.reset();
				changeUploadButtonText(uploadButton, 'Choose File');
				if(imgComponent) {
					imgComponent.container.remove();
				}
				status = 'unattached';
			});
		}
	});

	var options = getOptions(uploadWidget);
	setConfig(options);
}
if (document.addEventListener) {
	document.addEventListener('DOMContentLoaded', initialize);	
}
