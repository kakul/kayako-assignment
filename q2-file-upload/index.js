var express = require('express')
var path = require('path')
var app = express()

app.use('/scripts', express.static(path.join(__dirname, 'scripts')))
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')))

app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname ,'views/index.html'))
})
app.post('/upload', function(req, res, next) {
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify({ success: true }));
	console.log(req);
});
app.listen(3000)